import styles from "./Previous.module.scss";

function Previous(props) {
  return <div className={styles.Previous}>{props.children}</div>;
}

export default Previous;
