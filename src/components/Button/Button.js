import styles from "./Button.module.scss";
import cx from "classnames";

function Button(props) {
  const btnClass = cx(styles.Button, {
    [styles.spanTwo]: props.gridSpan,
    [styles.operation]: props.operation,
    [styles.controls]: props.controls,
    [styles.equals]: props.equals,
    [styles.point]: props.point,
  });

  let symbol;

  switch (props.children) {
    case "divide":
      symbol = `\u00F7`;
      break;
    case "x":
      symbol = `\u00D7`;
      break;
    case "+":
      symbol = `\u002B`;
      break;
    case "-":
      symbol = `\u2212`;
      break;
    case "=":
      symbol = `\u003D`;
      break;
    default:
      symbol = props.children;
  }

  return (
    <div
      className={btnClass}
      onClick={(event) => {
        props.eventHandler(event, props.type, props.name);
      }}
    >
      {symbol.toUpperCase()}
    </div>
  );
}

export default Button;
