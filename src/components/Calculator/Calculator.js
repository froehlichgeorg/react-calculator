import styles from "./Calculator.module.scss";
import React from "react";

const Calculator = (props) => {
  return <div className={styles.Calculator}>{props.children}</div>;
};

export default Calculator;
