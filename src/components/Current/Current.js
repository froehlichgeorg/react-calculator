import styles from "./Current.module.scss";

function Current(props) {
  return <div className={styles.Current}>{props.children}</div>;
}

export default Current;
