import './MemoryModal.scss'
import React, {useCallback, useEffect, useRef} from "react";

const MemoryModal = ({setCurrentOperation, setCurrent, showModal, setShowModal, setPrevious}) => {
    const data = JSON.parse(window.localStorage.getItem('expressions'))
    const ref = useRef(null);

    const historyHandler = (event) => {
        const itemId = event.currentTarget.dataset.id

        setPrevious(data[itemId].previous)
        setCurrent(data[itemId].current)
        setCurrentOperation(data[itemId].currentOperation)
        setShowModal(false)
    }

    const handleClickOutside = useCallback((event) => {
        if (ref.current && !ref.current.contains(event.target)) {
            setShowModal(false)
        }
    }, [setShowModal])

    useEffect(() => {
        document.addEventListener('click', handleClickOutside, true);
    },[handleClickOutside])

    return (<div className='modal-overlay'>
        <div className="modal-window"  ref={ref}>
            <div className="results">
                <table className="results-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Value 1</th>
                            <th>Operation</th>
                            <th>Value 2</th>
                            <th>Result</th>
                        </tr>
                    </thead>
                    <tbody>
                    {data.map((item, key) =>
                    key <= 9 ? <tr onClick={historyHandler} data-id={key} key={key}>
                        <td>{key+1}</td>
                        <td>{item.current}</td>
                        <td>{item.currentOperation}</td>
                        <td>{item.previous}</td>
                        <td>{item.result}</td>
                    </tr> : null
                    )}
                    </tbody>
                </table>
            </div>
            <div className='close' onClick={() => setShowModal(false)}></div>
        </div>

    </div>)
}

export default MemoryModal




