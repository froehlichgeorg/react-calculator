export const calsObject = [
  {
    name: "ac",
    gridSpan: true,
    controls: true,
    type: "clear",
  },
  {
    name: "del",
    controls: true,
    type: "delete",
  },
  {
    name: `divide`,
    operation: true,
    type: "operation",
  },
  {
    name: "7",
    type: "number",
  },
  {
    name: "8",
    type: "number",
  },
  {
    name: "9",
    type: "number",
  },
  {
    name: "x",
    operation: true,
    type: "operation",
  },
  {
    name: "4",
    type: "number",
  },
  {
    name: "5",
    type: "number",
  },
  {
    name: "6",
    type: "number",
  },
  {
    name: "+",
    operation: true,
    type: "operation",
  },
  {
    name: "1",
    type: "number",
  },
  {
    name: "2",
    type: "number",
  },
  {
    name: "3",
    type: "number",
  },
  {
    name: "-",
    operation: true,
    type: "operation",
  },
  {
    name: ".",
    controls: true,
    point: true,
    type: "point",
  },
  {
    name: "0",
    type: "number",
  },
  {
    name: "M",
    controls: true,
    type: "history",
  },
  {
    name: "=",
    gridSpan: false,
    equals: true,
    type: "equals",
  },

];
