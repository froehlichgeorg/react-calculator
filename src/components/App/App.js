import styles from "./App.module.scss";
import Calculator from "../Calculator";
import Container from "../Container/Container";
import Screen from "../Screen/Screen";
import Previous from "../Previous/Previous";
import Current from "../Current/Current";
import { useEffect, useState } from "react";
import { calsObject } from "./calcObject";
import Button from "../Button/Button";
import { v4 as uuidv4 } from "uuid";
import { OnlineStatusProvider } from "../../utilities/useOnlineStatus";
import MemoryModal from "../MemoryModal/MemoryModal";

function App() {
  const [previous, setPrevious] = useState("");
  const [current, setCurrent] = useState("");
  const [currentOperation, setCurrentOperation] = useState("");
  const [operationToShow, setOperationToShow] = useState("");
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    window.localStorage.setItem("expressions", []);
  }, []);

  const eventHandler = (event, type, name) => {
    switch (type) {
      case "number":
        currentHandler(name);
        break;
      case "point":
        !current.includes(".") && currentHandler(".");
        break;
      case "clear":
        clearAllHandler();
        break;
      case "delete":
        setCurrent(current.slice(0, -1));
        break;
      case "operation":
        operationHandler(name);
        break;
      case "equals":
        setPrevious("");
        setOperationToShow("");
        setCurrentOperation("");
        setCurrent(calculate());
        break;
      case "history":
        window.localStorage.getItem("expressions").length && setShowModal(true);
        break;
      default:
        return null;
    }
  };

  const operationHandler = (operationName) => {
    if (!current) return;

    if (operationName) {
      changeOperationToShow(operationName);
      setCurrentOperation(operationName);
      setPrevious(current);
      setCurrent("");
      if (current && previous) {
        setPrevious(calculate());
      }
    }
  };

  const storeOperation = (result) => {
    if (window.localStorage.getItem("expressions")) {
      let currentState = JSON.parse(window.localStorage.getItem("expressions"));
      const newItems = [
        {
          current: current,
          currentOperation: currentOperation,
          previous: previous,
          result: result,
        },
        ...currentState,
      ];

      window.localStorage.setItem("expressions", JSON.stringify(newItems));
    } else {
      window.localStorage.setItem(
        "expressions",
        JSON.stringify([
          {
            current: current,
            currentOperation: currentOperation,
            previous: previous,
            result: result,
          },
        ])
      );
    }
  };

  const calculate = () => {
    let result;
    switch (currentOperation) {
      case "+":
        result = +previous + +current;
        break;
      case "-":
        result = +previous - +current;
        break;
      case "x":
        result = +previous * +current;
        break;
      case "divide":
        result = +previous / +current;
        break;
      default:
        result = 0;
    }

    storeOperation(result);
    return result.toString();
  };

  const changeOperationToShow = (operationName) => {
    switch (operationName) {
      case "+":
        setOperationToShow("+");
        break;
      case "-":
        setOperationToShow("-");
        break;
      case "divide":
        setOperationToShow("/");
        break;
      case "x":
        setOperationToShow("x");
        break;
      default:
        return null;
    }
  };

  const clearAllHandler = () => {
    setCurrent("");
    setPrevious("");
    setCurrentOperation("");
    setOperationToShow("");
  };

  const currentHandler = (value) => {
    setCurrent(current + value);
  };

  return (
    <OnlineStatusProvider>
      {!showModal ? (
        <div className={styles.App}>
          <Container>
            <Calculator>
              <Screen>
                <Previous>
                  {previous}
                  {operationToShow}
                </Previous>
                <Current>{current}</Current>
              </Screen>
              {calsObject.map((button) => {
                return (
                  <Button
                    gridSpan={button.gridSpan}
                    controls={button.controls}
                    operation={button.operation}
                    point={button.point}
                    equals={button.equals}
                    key={uuidv4()}
                    type={button.type}
                    eventHandler={eventHandler}
                    name={button.name}
                  >
                    {button.name}
                  </Button>
                );
              })}
            </Calculator>
          </Container>
        </div>
      ) : (
        <MemoryModal
          setShowModal={setShowModal}
          showModal={showModal}
          setCurrentOperation={setCurrentOperation}
          setCurrent={setCurrent}
          setPrevious={setPrevious}
        />
      )}
    </OnlineStatusProvider>
  );
}

export default App;
