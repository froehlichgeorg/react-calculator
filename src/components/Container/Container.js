import styles from "./Container.module.scss";
import { useOnlineStatus } from "../../utilities/useOnlineStatus";

function Container(props) {
  const isOnline = useOnlineStatus();

  return (
    <div>
      {isOnline !== false ? (
        <div className={styles.Container}>{props.children}</div>
      ) : (
        "App is offline"
      )}
    </div>
  );
}

export default Container;
